<?php
namespace HIVE\HiveExtContactdata\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Perrin Ennen <p.ennen@teufels.com>
 */
class ContactDataTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtContactdata\Domain\Model\ContactData
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtContactdata\Domain\Model\ContactData();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendtitle()
        );
    }

    /**
     * @test
     */
    public function setBackendtitleForStringSetsBackendtitle()
    {
        $this->subject->setBackendtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPhoneReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getPhone()
        );
    }

    /**
     * @test
     */
    public function setPhoneForStringSetsPhone()
    {
        $this->subject->setPhone('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'phone',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFaxReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFax()
        );
    }

    /**
     * @test
     */
    public function setFaxForStringSetsFax()
    {
        $this->subject->setFax('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fax',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMobileReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMobile()
        );
    }

    /**
     * @test
     */
    public function setMobileForStringSetsMobile()
    {
        $this->subject->setMobile('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mobile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail()
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'email',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWebsiteReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getWebsite()
        );
    }

    /**
     * @test
     */
    public function setWebsiteForStringSetsWebsite()
    {
        $this->subject->setWebsite('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'website',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFacebookReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFacebook()
        );
    }

    /**
     * @test
     */
    public function setFacebookForStringSetsFacebook()
    {
        $this->subject->setFacebook('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'facebook',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTwitterReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTwitter()
        );
    }

    /**
     * @test
     */
    public function setTwitterForStringSetsTwitter()
    {
        $this->subject->setTwitter('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'twitter',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getGoogleplusReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getGoogleplus()
        );
    }

    /**
     * @test
     */
    public function setGoogleplusForStringSetsGoogleplus()
    {
        $this->subject->setGoogleplus('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'googleplus',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLinkedinReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLinkedin()
        );
    }

    /**
     * @test
     */
    public function setLinkedinForStringSetsLinkedin()
    {
        $this->subject->setLinkedin('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'linkedin',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInstagramReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInstagram()
        );
    }

    /**
     * @test
     */
    public function setInstagramForStringSetsInstagram()
    {
        $this->subject->setInstagram('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'instagram',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getXingReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getXing()
        );
    }

    /**
     * @test
     */
    public function setXingForStringSetsXing()
    {
        $this->subject->setXing('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'xing',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWechatReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getWechat()
        );
    }

    /**
     * @test
     */
    public function setWechatForStringSetsWechat()
    {
        $this->subject->setWechat('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'wechat',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSkypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSkype()
        );
    }

    /**
     * @test
     */
    public function setSkypeForStringSetsSkype()
    {
        $this->subject->setSkype('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'skype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getYoutubeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getYoutube()
        );
    }

    /**
     * @test
     */
    public function setYoutubeForStringSetsYoutube()
    {
        $this->subject->setYoutube('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'youtube',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }
}
