<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtContactdata',
            'Hivecontactdatalistcontactdata',
            'hive :: ContactData :: listContactData'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_contactdata', 'Configuration/TypoScript', 'hive_ext_contactdata');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextcontactdata_domain_model_contactdata', 'EXT:hive_ext_contactdata/Resources/Private/Language/locallang_csh_tx_hiveextcontactdata_domain_model_contactdata.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextcontactdata_domain_model_contactdata');

    }
);
