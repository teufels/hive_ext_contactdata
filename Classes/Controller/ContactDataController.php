<?php
namespace HIVE\HiveExtContactdata\Controller;

/***
 *
 * This file is part of the "hive_ext_contactdata" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *
 ***/

/**
 * ContactDataController
 */
class ContactDataController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * contactDataRepository
     *
     * @var \HIVE\HiveExtContactdata\Domain\Repository\ContactDataRepository
     * @inject
     */
    protected $contactDataRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $contactDatas = $this->contactDataRepository->findAll();
        $this->view->assign('contactDatas', $contactDatas);
    }
}
