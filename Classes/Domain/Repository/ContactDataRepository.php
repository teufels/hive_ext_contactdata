<?php
namespace HIVE\HiveExtContactdata\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_contactdata" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for ContactDatas
 */
class ContactDataRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
